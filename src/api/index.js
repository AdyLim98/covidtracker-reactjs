import axios from 'axios';

const url = "https://covid19.mathdro.id/api";

//if no country become undefined and will no go in the if condition but still can run
//so undefined also can make it run!!!
export const fetchData = async (country) =>{
    let changeUrl = url

    if(country){
        changeUrl =url+"/countries/"+country
    }

    try{
        // const response = await  axios.get(url);
        // return response.data;
        const { data: {confirmed,recovered,deaths,lastUpdate} } = await axios.get(changeUrl)
        const modifiedData ={
            "confirmed":confirmed,
            "recovered":recovered,
            "deaths":deaths,
            "lastUpdate":lastUpdate
        }
        return modifiedData
    }catch(error){
        console.log(error);
    }
}

export const fetchDailyData = async() =>{
    try{
      const {data} = await axios.get(url+"/daily")
      //due to want to become object so ({})
      const modifiedDaily = data.map((daily)=>({
        confirmed:daily.confirmed.total,
        deaths:daily.deaths.total,
        date:daily.reportDate
      }))

      return modifiedDaily
    }catch(error){
        console.log(error)
    }
}

export const fetchCountries = async() =>{
    try{
       const {data:{countries}} = await axios.get(url+"/countries")
       return countries.map((country)=> country.name)
       
    }catch(error){
        console.log(error)
    }
}