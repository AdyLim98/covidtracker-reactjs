import React from 'react';

//Method 1
// import Card from '../components/Card/card';
// import Chart from '../components/Chart/chart';
// import Country from '../components/Country/country';

//Method 2 | work with export default class defined in index.js so the code looks more clean
import { Card , Chart , Country } from './components';
import styles from './App.module.css';
import {fetchData} from './api';

export default class App extends React.Component{

    state = {
        data:{},
        country:'',
        // countryDataCombined:{}
    }

    async componentDidMount(){
        const coronaReport = await fetchData()
        this.setState({data:coronaReport})
        console.log(coronaReport); 
    }
    //imagine the handleCountryChange method is at the above of the Country code component there ,and no nid import the fetchData again in that country page code
    handleCountryChange = async(country) =>{
        const countryReport = await fetchData(country)
        this.setState({data:countryReport , country:country})
        // this.state.countryDataCombined = {
        //     data:this.state.data,
        //     country:this.state.country
        // } 
        // console.log(this.state.countryDataCombined)
    }

    render(){
        return(
            <div className={styles.container}>
                <Card data={this.state.data}/>
                <Country changeValue={this.handleCountryChange}/>
                <Chart data={this.state.data} country={this.state.country}/>
            </div>
        )
    }
}