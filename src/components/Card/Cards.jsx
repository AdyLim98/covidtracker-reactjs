import React from 'react';
import {Card ,CardContent,Typography,Grid} from '@material-ui/core';
//dependencies for animation
import CountUpAnimation from 'react-countup';
//dependencies for multiple styles inline
import cx from 'classnames';
import styles from './Card.module.css';

const Cards = ({data:{confirmed,recovered,deaths,lastUpdate}}) => {
    console.log("here:",lastUpdate)
    const date = (new Date(lastUpdate)).toLocaleString()
    if(!confirmed){
        return(
        <h1>Loading</h1>
        )
    }
    return(
        <div className={styles.container}>
            <Grid container spacing={300} justify="center">
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card,styles.infected)}>
                    <CardContent>
                        <Typography color="textSecondary" gutterBottom>Infected</Typography>
                        <Typography variant="h5">
                            <CountUpAnimation start={0}
                                end={confirmed.value}
                                duration={2.5}
                                separator=","/>
                        </Typography>
                        <Typography color="textSecondary">{date}</Typography>
                        <Typography variant="body2">Number of active cases of COVID-19</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card,styles.recoveries)}>
                    <CardContent>
                        <Typography color="textSecondary" gutterBottom>Recoveries</Typography>
                        <Typography variant="h5">
                            <CountUpAnimation start={0}
                                    end={recovered.value}
                                    duration={2.5}
                                    separator=","/>
                        </Typography>
                        <Typography color="textSecondary">{date}</Typography>
                        <Typography variant="body2">Number of recoveries cases of COVID-19</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card,styles.deaths)}>
                    <CardContent>
                        <Typography color="textSecondary" gutterBottom>Deaths</Typography>
                        <Typography variant="h5">
                            <CountUpAnimation start={0}
                                    end={deaths.value}
                                    duration={2.5}
                                    separator=","/>
                        </Typography>
                        <Typography color="textSecondary">{date}</Typography>
                        <Typography variant="body2">Number of deaths cases of COVID-19</Typography>
                    </CardContent>
                </Grid>
            </Grid>
        </div>
    )
}

export default Cards;